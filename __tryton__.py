# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Event Reminder',
    'name_de_DE': 'Parteien Ereignisse Wiedervorlage',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Allows to schedule reminder for events.
    ''',
    'description_de_DE': '''Ermöglicht die Wiedervorlage von Ereignissen.
    ''',
    'depends': [
        'party_event',
        'reminder',
    ],
    'xml': [
        'event.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
