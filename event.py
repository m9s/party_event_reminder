# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval


class Event(ModelSQL, ModelView):
    _name = 'party.event'

    reminders = fields.Function(fields.One2Many('reminder.reminder', None,
        'Reminders', context={
            'reminder_reference_model': 'party.event',
            'reminder_reference_id': Eval('active_id'),
            'reminder_base_time': Eval('time'),
            }),
        'get_reminders', setter='set_reminders')

    def get_reminders(self, ids, name):
        reminder_obj = Pool().get('reminder.reminder')
        res = {}
        for event_id in ids:
            res[event_id] = reminder_obj.search([('reference', '=',
                'party.event,' + str(event_id))])
        return res

    def set_reminders(self, ids, name, value):
        reminder_obj = Pool().get('reminder.reminder')
        events = self.browse(ids)
        currents = [reminder.id for event in events for reminder in
            event.reminders]
        if not value:
            return
        for v in value:
            to_unlink = []
            to_link = []
            operator = v[0]

            target_ids = len(v) > 1 and v[1] or []
            if operator == 'create':
                vals = v[1]
                vals['reference'] = 'party.event,%s' % ids[0]
                reminder_obj.create(vals)
            elif operator == 'write':
                reminder_obj.write(v[1], v[2])
            elif operator == 'delete':
                reminder_obj.delete(v[1])
            elif operator == 'delete_all':
                reminder_obj.delete(currents)
            elif operator == 'unlink':
                to_unlink.extend((i for i in target_ids if i in currents))
            elif operator == 'add':
                to_link.extend((i for i in target_ids if i not in
                    currents))
            elif operator == 'unlink_all':
                to_unlink.extend(currents)
            elif operator == 'set':
                to_link.extend((i for i in target_ids
                    if i not in currents))
                to_unlink.extend((i for i in currents
                    if i not in target_ids))
            else:
                raise Exception('Operation not supported')
            reminder_obj.write(to_unlink, {'reference': False})
            if to_link:
                reminder_obj.write(to_link, {'reference': 'party.event,%s' % ids[0]})
        return

    def get_reminder_base_time(self, event_id):
        event = self.browse(event_id)
        return event.time

Event()

